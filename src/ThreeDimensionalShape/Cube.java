package ThreeDimensionalShape;


import TwoDimensionalShape.Square;

public class Cube extends Square {

    public Cube(double lengthOfSide) {
        super(lengthOfSide);
    }


    public double area(){
        return super.area();
    }
    public double perimeter() {
        return (super.perimeter() * 3);
    }

    public double Volume()
    {
        return (super.area());
    }

    @Override
    public String toString(){
        return "side: "+lengthOfSide;
    }


}

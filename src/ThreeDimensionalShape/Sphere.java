package ThreeDimensionalShape;

import TwoDimensionalShape.Circle;

public class Sphere extends Circle {

    public Sphere(double radius) {
        super((int) radius);

    }
    public double area(){
        int radius = 0;
        return 4*Math.PI*radius*radius;
    }
    public double volume(){
        int radius = 0;
        return 4*Math.PI*radius*radius*radius/3;
    }

    public double perimeter(){
        int radius = 0;
        return 2*Math.PI*radius;
    }


    @Override
    public String toString() {
        String radius = null;
        return "Sphere{" +
                "radius=" + radius +
                '}';
    }
}
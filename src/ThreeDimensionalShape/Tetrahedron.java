package ThreeDimensionalShape;

public class Tetrahedron {

    public double side;

    public Tetrahedron(double side) {
        this.side = side;
    }

    public double area(){
        return side*side*Math.sqrt(3);

    }

    public double volume(){
        return side*side*side*Math.sqrt(2)/12;
    }

    public double perimeter(){
        return side*6;
    }

    @Override
    public String toString() {
        return "Tetrahedron{" + "side=" + side + '}';
    }
}
import ThreeDimensionalShape.Cube;
import ThreeDimensionalShape.Sphere;
import ThreeDimensionalShape.Tetrahedron;
import TwoDimensionalShape.Circle;
import TwoDimensionalShape.Square;
import TwoDimensionalShape.Triangle;
import Shapes.Shape;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Tester {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner input = new Scanner(new File("C:\\Users\\lenovo\\IdeaProjects\\LAB9\\out\\production\\LAB9\\instructor.txt"));
        Shape shape;
        ArrayList<Shape> shapes = new ArrayList<Shape>();

        Circle c = new Circle(5);
        while (input.hasNextLine()) {
            String line = input.nextLine();
            String[] tokens = line.split(" ");
            if (tokens[0].equals("O") && tokens[1].equals("C")) {

                double radius = Double.parseDouble(tokens[2]);
                shape = new Shape(radius);
                shapes.add(shape);
                System.out.println(shape);



            } else if (tokens[0].equals("O") && tokens[1].equals("S")) {
                double side = Double.parseDouble(tokens[2]);
                shape = new Shape(side);
                shapes.add(shape);
                System.out.println(shape);


            } else if (tokens[0].equals("O") && tokens[1].equals("T")) {
                double base = Double.parseDouble(tokens[2]);
                double side1 = Double.parseDouble(tokens[3]);
                double side2 = Double.parseDouble(tokens[4]);
                double height = Double.parseDouble(tokens[5]);
                shape = new Shape(base, side1, side2, height);
                shapes.add(shape);
                System.out.println(shape);


            }
            else if (tokens[0].equals("O") && tokens[1].equals("SP")) {
                double side = Double.parseDouble(tokens[2]);
                shape = new Shape(side);
                shapes.add(shape);
                System.out.println(shape);
            }
            else if (tokens[0].equals("O") && tokens[1].equals("CU")) {
                double side = Double.parseDouble(tokens[2]);
                shape = new Shape(side);
                shapes.add(shape);
                System.out.println(shape);


            } else if (tokens[0].equals("O") && tokens[1].equals("TE")) {
                double side = Double.parseDouble(tokens[2]);
                shape = new Shape(side);
                shapes.add(shape);
                System.out.println(shape);


            } else if (tokens[0].equals("TA")) {

                double totalarea = 0;
                for (int i = 1; i < shapes.size(); i++) {

                    totalarea += shapes.get(i).area();

                }
                System.out.println("total area: "+ totalarea);


            }

            else if (tokens[0].equals("TP")) {

                double totalperimeter = 0;
                for (int i = 1; i < shapes.size(); i++) {

                    totalperimeter += shapes.get(i).perimeter();

                }
                System.out.println("total perimeter: "+totalperimeter);


            }

            else if (tokens[0].equals("TV")) {

                double totalvolume = 0;
                for (int i = 1; i < shapes.size(); i++) {

                    totalvolume += shapes.get(i).volume();

                }
                System.out.println("total volume: "+totalvolume);


            }


        }


    }
}